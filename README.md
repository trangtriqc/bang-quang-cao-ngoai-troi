# Giới thiệu về công ty trang trí quảng cáo 3M

Được thành lập vào năm 2014, Trang trí quảng cáo 3M DECOR mong muốn đem lại những giá trị tốt nhất cho doanh nghiệp. Với các lĩnh vực nền tảng cốt lõi: làm biển quảng cáo, thi công mặt dựng alu, thi công gian hàng hội chợ,...

## Thông tin liên hệ

Trang trí quảng cáo

Phone: 0977689733

Address: 28 Phan Huy Ích, Phường 12, Gò Vấp, Hồ Chí Minh 700000, Vietnam

Website: https://trangtriquangcao.com

Facebook: https://www.facebook.com/trangtriqc

Twitter: https://twitter.com/trangtriqc

# Các dịch vụ tại công ty trang trí quảng cáo

- Thiết kế thi công làm bảng quảng cáo chuyên nghiệp
- Thi công ốp mặt dựng alu cao cấp
- Tạo biển tên phòng ban
- Thiết kế thi công các loại chữ nổi